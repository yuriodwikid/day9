package Day9.Assignment4;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;
import java.io.IOException;

public class Assignment4Server {
    public static void main(String[] args) throws Exception {
        String port = "";
        Scanner sc = new Scanner(System.in);

        try {
            try (InputStream input = new FileInputStream("/Users/ada-nb185/Documents/config.txt")) {

                Properties prop = new Properties();

                // load a properties file
                prop.load(input);

                // get the property value and print it out

                port = prop.getProperty("port");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            ServerSocket ss = new ServerSocket(Integer.parseInt(port));
            Socket s = ss.accept();//establishes connection

            DataInputStream dis = new DataInputStream(s.getInputStream());
            DataOutputStream dout = new DataOutputStream(s.getOutputStream());

            String msg = "";
            while (!msg.equalsIgnoreCase("99")) {

                //Receive
                System.out.println("Menunggu client...");
                msg = (String) dis.readUTF();
                System.out.println("Client said= " + msg);

                if (msg.equalsIgnoreCase("99")) {
                    System.out.println("Client out");
                    break;

                } else if (msg.equalsIgnoreCase("1")) {
                    int data = 0;
                    String strdata = "";
                    try (FileReader fileReader = new FileReader("/Users/ada-nb185/Documents/IdeaProjects/Day9/src/Day9/fileJSON.txt")) {
                        while ((data = fileReader.read()) != -1) {
                            strdata += (char) data;
                        }
                        fileReader.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    dout.writeUTF(strdata);
                    dout.flush();
                } else if (msg.equalsIgnoreCase("4")) {
                    ss.close();
                    break;
                }
            }
        }   catch (Exception e) {
            System.out.println(e);
        }
    }
}





