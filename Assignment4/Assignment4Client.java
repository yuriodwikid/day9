package Day9.Assignment4;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;
import java.io.IOException;

public class Assignment4Client {

    public static void main(String[] args) throws Exception{
        String ipProp = "";
        String portProp = "";
        Scanner sc = new Scanner(System.in);
        ArrayList<MahasiswaLama> alMaha = new ArrayList<>();
        MahasiswaLama es;

        try {
            try (InputStream input = new FileInputStream("/Users/ada-nb185/Documents/config.txt")) {
                Properties prop = new Properties();

                // load a properties file
                prop.load(input);

                // get the property value and print it out
                ipProp = prop.getProperty("ip");
                portProp = prop.getProperty("port");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            Socket s = new Socket(ipProp, Integer.parseInt(portProp));
            DataInputStream dis = new DataInputStream(s.getInputStream());
            DataOutputStream dout = new DataOutputStream(s.getOutputStream());
            String pesan = "";
            String dataMaha = "";

            while (!pesan.equalsIgnoreCase("EXIT")) {

                System.out.println("MENU");
                System.out.println("1. Connect Socket");
                System.out.println("2. Create FileProses.txt - Decode JSON");
                System.out.println("3. Tulis  ke File ");
                System.out.println("99. EXIT");
                System.out.println("INPUT NOMOR: ");

                //Send
                pesan = sc.next();
                dout.writeUTF(pesan);
                dout.flush();

                //Receive
                if (pesan.equalsIgnoreCase("99")) {
                    System.out.println("Server out");
                    break;
                } else if (pesan.equalsIgnoreCase("1")) {
                    dataMaha = (String) dis.readUTF();
                    System.out.println(dataMaha);
                } else if (pesan.equalsIgnoreCase("2")) {
                    try {
                        JSONParser parser = new JSONParser();
                        Reader reader = new StringReader(dataMaha);
                        Object mahasiswaObject = parser.parse(reader);

                        ArrayList<JSONObject> jalMahasiswa = (ArrayList<JSONObject>) mahasiswaObject;
                        FileWriter fw = new FileWriter("/Users/ada-nb185/Documents/IdeaProjects/Day9/src/Day9/Assignment4/FileProsesJSONnew.txt");

                        for (JSONObject et : jalMahasiswa) {
                            String nama = (String) et.get("nama");
                            fw.write("Nama : " + nama + "\n");
                            long nilaiFisika = (long) et.get("nilaiFisika");
                            fw.write("Nilai Fisika : " + nilaiFisika + "\n");
                            long nilaiKimia = (long) et.get("nilaiKimia");
                            fw.write("Nilai Kimia : " + nilaiKimia + "\n");
                            long nilaiBiologi = (long) et.get("nilaiBiologi");
                            fw.write("Nilai Biologi : " + nilaiBiologi + "\n");

                            MahasiswaLama ex = new MahasiswaLama(nama, (int) nilaiFisika, (int) nilaiKimia, (int) nilaiBiologi);
                            alMaha.add(ex);
                        }
                        fw.close();

                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
                    else if (pesan.equalsIgnoreCase("3")) {
                    try {
                        FileWriter fw = new FileWriter("/Users/ada-nb185/Documents/IdeaProjects/Day9/src/Day9/Assignment4/FileRata2JSONnew.txt");
                        JSONArray almaha2 = new JSONArray();

                        for (MahasiswaLama x : alMaha) {
                            JSONObject objMahasiswaLama = new JSONObject();
                            objMahasiswaLama.put("nama", x.getNama());
                            objMahasiswaLama.put("nilaiRata2", ((x.getNilaiFisika() + x.getNilaiKimia() + x.getNilaiBiologi())/3));
                            almaha2.add(objMahasiswaLama);
                        }
                        fw.write(String.valueOf(almaha2));
                        fw.close();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                } else if (pesan.equalsIgnoreCase("99")) {
                    dout.close();
                    s.close();
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

