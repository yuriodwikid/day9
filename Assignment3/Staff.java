package Day9.Assignment3;

import java.security.InvalidKeyException;
import java.util.ArrayList;

public class Staff extends Worker{
    long tMakan;
    ArrayList<String> email = new ArrayList<>();

    public long gettMakan() {
        return tMakan;
    }

    public void settMakan(long tMakan) {
        this.tMakan = tMakan;
    }

    public ArrayList<String> getEmail() {
        return email;
    }

    public void setEmail(ArrayList<String> email) {
        this.email = email;
    }

    public Staff(int IDKaryawan, String Nama, long tPulsa, long gajiPokok, int absensiHari, long tMakan, ArrayList<String> email){
        super(IDKaryawan, Nama, tPulsa, gajiPokok, absensiHari);
        this.tMakan = tMakan;
        this.email = email;
    }


}
