package Day9.Assignment3;

abstract class Worker {
    int IDKaryawan;
    String Nama;
    long tPulsa;
    long gajiPokok;
    int absensiHari;

    public long getGajiPokok() {
        return gajiPokok;
    }

    public void setGajiPokok(long gajiPokok) {
        this.gajiPokok = gajiPokok;
    }

    public int getIDKaryawan() {
        return IDKaryawan;
    }

    public void setIDKaryawan(int IDKaryawan) {
        this.IDKaryawan = IDKaryawan;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public long gettPulsa() {
        return tPulsa;
    }

    public void settPulsa(long tPulsa) {
        this.tPulsa = tPulsa;
    }

    public int getAbsensiHari() {
        return absensiHari;
    }

    public void setAbsensiHari(int absensiHari) {
        this.absensiHari = absensiHari;
    }
    public Worker(int IDKaryawan, String Nama, long tPulsa, long gajiPokok, int absensiHari) {
        this.IDKaryawan = IDKaryawan;
        this.Nama = Nama;
        this.tPulsa = tPulsa;
        this.gajiPokok = gajiPokok;
        this.absensiHari = absensiHari;
    }
}
