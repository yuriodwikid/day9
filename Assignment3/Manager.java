package Day9.Assignment3;

import java.util.ArrayList;

public class Manager extends Worker{
    long tTransport;
    long tEntertainment;
    ArrayList<String> telepon = new ArrayList<>();

    public long gettTransport() {
        return tTransport;
    }

    public void settTransport(long tTransport) {
        this.tTransport = tTransport;
    }

    public long gettEntertainment() {
        return tEntertainment;
    }

    public void settEntertainment(long tEntertainment) {
        this.tEntertainment = tEntertainment;
    }

    public ArrayList<String> getTelepon() {
        return telepon;
    }

    public void setTelepon(ArrayList<String> telepon) {
        this.telepon = telepon;
    }

    public Manager(int IDKaryawan, String Nama, long tPulsa, long gajiPokok, int absensiHari,
                    long tTransport, long tEntertainment, ArrayList<String> telepon){
        super(IDKaryawan, Nama, tPulsa, gajiPokok, absensiHari);
        this.telepon = telepon;
        this.tTransport = tTransport;
        this.tEntertainment = tEntertainment;
    }


}
