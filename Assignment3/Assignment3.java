package Day9.Assignment3;

import Day9.Assignment3.Staff;
import Day9.Assignment3.Manager;
import Day9.Assignment3.Worker;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Assignment3 {

    public static void main(String[] args) throws Exception {
        int nomor = 0;
        int nomor2 = 0;
        int absensiHari = 20;
        Staff karyawan;
        Manager karyawan2;
        Scanner input = new Scanner(System.in);
        ArrayList<Staff> alStaff = new ArrayList<>();
        ArrayList<Manager> alManager = new ArrayList<>();
        while (nomor != 99) {
            System.out.println("MENU");
            System.out.println("1. Buat Staff or Manager");
            System.out.println("2. Create JSON Format and Write to File");
            System.out.println("3. Read JSON Format from a File, input Filename");
            System.out.println("99.EXIT");
            System.out.println("INPUT NOMOR: ");
            nomor = input.nextInt();

            switch (nomor) {
                case 1:
                    System.out.println("Jika ingin input data staff tekan 1 dan manager tekan 2");
                    nomor2 = input.nextInt();
                    if (nomor2 == 1) {
                        System.out.println("Input ID Karyawan: ");
                        int IDKaryawan = input.nextInt();
                        System.out.println("Input Nama: ");
                        String Nama = input.next();
                        System.out.println("Tunjangan Pulsa: ");
                        Long tPulsa = input.nextLong();
                        System.out.println("Gaji Pokok: ");
                        Long gajiPokok = input.nextLong();
                        System.out.println("Absensi anda: " + absensiHari);
                        System.out.println("Masukkan tunjangan makan: ");
                        Long tMakan = input.nextLong();
                        ArrayList<String> emailList = new ArrayList<>();
                        String pilihan = "y";
                        while (pilihan.equalsIgnoreCase("y")) {
                            System.out.println("Masukkan emailmu: ");
                            String email = input.next();
                            emailList.add(email);
                            System.out.println("Apakah anda ingin menginput email kembali? (y/n) ");
                            pilihan = input.next();
                        }
                        karyawan = new Staff(IDKaryawan, Nama, tPulsa, gajiPokok, absensiHari, tMakan, emailList);
                        alStaff.add(karyawan);
                    } else if (nomor2 == 2) {
                        System.out.println("Input ID Karyawan: ");
                        int IDKaryawan = input.nextInt();
                        System.out.println("Input Nama: ");
                        String Nama = input.next();
                        System.out.println("Tunjangan Pulsa: ");
                        Long tPulsa = input.nextLong();
                        System.out.println("Gaji Pokok: ");
                        Long gajiPokok = input.nextLong();
                        System.out.println("Absensi anda: " + absensiHari);
                        System.out.println("Masukkan tunjangan transport: ");
                        Long tTransport = input.nextLong();
                        System.out.println("Masukkan tunjangan transport: ");
                        Long tEntertainment = input.nextLong();
                        ArrayList<String> teleponList = new ArrayList<>();
                        String pilihan2 = "y";
                        while (pilihan2.equalsIgnoreCase("y")) {
                            System.out.println("Masukkan teleponmu: ");
                            String email = input.next();
                            teleponList.add(email);
                            System.out.println("Apakah anda ingin menginput telepon kembali? (y/n) ");
                            pilihan2 = input.next();
                        }
                        karyawan2 = new Manager(IDKaryawan, Nama, tPulsa, gajiPokok, absensiHari, tTransport, tEntertainment, teleponList);
                        alManager.add(karyawan2);
                    }
                    break;
                case 2:
                    try {
                        FileWriter fw = new FileWriter("/Users/ada-nb185/Documents/IdeaProjects/Day9/src/Day9/Assignment3/filestaff.txt");
                        JSONArray alStaff2 = new JSONArray();

                        for (Staff s : alStaff) {
                            JSONObject objStaff = new JSONObject();
                            objStaff.put("id", s.getIDKaryawan());
                            objStaff.put("nama", s.getNama());
                            objStaff.put("tunjangan pulsa", s.gettPulsa());
                            objStaff.put("gaji pokok", s.getGajiPokok());
                            objStaff.put("absensi", s.getAbsensiHari());
                            objStaff.put("tunjangan makan", s.gettMakan());

                            JSONArray alEmail = new JSONArray();
                            for (String e : s.getEmail()) {
                                alEmail.add(e);
                            }
                            objStaff.put("email", alEmail);
                            alStaff2.add(objStaff);
                        }
                        fw.write(String.valueOf(alStaff2));
                        fw.close();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    System.out.println("Success...");

                    try {
                        FileWriter fw = new FileWriter("/Users/ada-nb185/Documents/IdeaProjects/Day9/src/Day9/Assignment3/filemanager.txt");
                        JSONArray alManager2 = new JSONArray();

                        for (Manager s : alManager) {
                            JSONObject objManager = new JSONObject();
                            objManager.put("id", s.getIDKaryawan());
                            objManager.put("nama", s.getNama());
                            objManager.put("tunjangan pulsa", s.gettPulsa());
                            objManager.put("gaji pokok", s.getGajiPokok());
                            objManager.put("absensi", s.getAbsensiHari());
                            objManager.put("tunjangan transport", s.gettTransport());
                            objManager.put("tunjangan entertainment", s.gettEntertainment());

                            JSONArray alTelepon = new JSONArray();
                            for (String e : s.getTelepon()) {
                                alTelepon.add(e);
                            }
                            objManager.put("telepon", alTelepon);
                            alManager2.add(objManager);
                        }
                        fw.write(String.valueOf(alManager2));
                        fw.close();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    System.out.println("Complete...");
                    break;

                case 3:
                    System.out.println("Masukkan nama file: ");
                    String namaFile = input.next();

                        FileReader fr = new FileReader("/Users/ada-nb185/Documents/IdeaProjects/Day9/src/Day9/Assignment3/" + namaFile);
                        String strResult = "";

                        int data = 0;
                        while ((data = fr.read()) != -1) {
                            strResult += (char) data;
                            }

                            JSONParser parser = new JSONParser();
                            Reader reader = new StringReader(strResult);

                            Object staffObject = parser.parse(reader);
                            ArrayList<JSONObject>jalStaff = (ArrayList) staffObject;
                            for (JSONObject jal : jalStaff) {
                                System.out.println("ID Karyawan: " + jal.get("id"));
                                System.out.println("Nama: " + jal.get("nama"));
                                System.out.println("Tunjang Pulsa: " + jal.get("tunjangan pulsa"));
                                System.out.println("Gaji Pokok: " +jal.get("gaji pokok"));
                                System.out.println("Absensi: " + jal.get("absensi"));

                                if(namaFile.equals("Staff.txt")) {
                                    System.out.println("Tunjangan Makan: " + jal.get("tunjangan makan"));

                                    ArrayList<String> jalEmail = (ArrayList<String>) jal.get("email");
                                    System.out.println("Email: ");
                                    for (String jam : jalEmail) {
                                        System.out.println(jam);
                                    }
                                    System.out.println();
                                }
                                else if(namaFile.equals("Manager.txt")) {
                                    System.out.println("Tunjangan Transport: " + jal.get("tunjangan transport"));
                                    System.out.println("Tunjangan Entertainment: " + jal.get("tunjangan entertainment"));

                                    ArrayList<String> jalTelepon = (ArrayList<String>) jal.get("telepon");
                                    System.out.println("Telepon: ");
                                    for (String jat : jalTelepon) {
                                        System.out.println(jat);
                                    }
                                    System.out.println();
                                }
                            }
                        System.out.println();
                        fr.close();
                        break;
                case 99:
                    break;
                    }
            }
        }
    }
