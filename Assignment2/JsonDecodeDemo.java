package Day9.Assignment2;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

public class JsonDecodeDemo {

    public static void main(String[] args) throws IOException, ParseException {

        String json1 = "{\"cars\":[{\"models\":[\"Fiesta\",\"Focus\",\"Mustang\"],\"name\":\"Ford\"},{\"models\":[\"320\",\"X3\",\"X5\"],\"name\":\"BMW\"},{\"models\":[\"500\",\"Panda\"],\"name\":\"Fiat\"}],\"name\":\"John\",\"age\":30}\n";
        JSONParser parser = new JSONParser();
        Reader reader = new StringReader(json1);

        Object jsonObj = parser.parse(reader);

        JSONObject jsonObject = (JSONObject) jsonObj;

        String name = (String) jsonObject.get("name");
        System.out.println("Name = " + name);

        long age = (long) jsonObject.get("age");
        System.out.println("Age = " + age);

        ArrayList<JSONObject> joMobil = (ArrayList<JSONObject>) jsonObject.get("cars");
        for (JSONObject car:joMobil) {
            String namaMobil = (String) car.get("name");
            System.out.println("Nama Mobil = " + namaMobil);

            ArrayList<String> models = (ArrayList<String>) car.get("models");
            System.out.println("Car models:");
            for (String model:models) {
                System.out.println(model);
            }
        }


//        String Cars = (String) jsonObject.get("Cars");
//        System.out.println("Cars = " + Cars);


    }
}
