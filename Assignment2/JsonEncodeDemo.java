package Day9.Assignment2;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
public class JsonEncodeDemo {
    public static void main(String[] args) {
//        JSONObject obj = new JSONObject();
//        obj.put("name", "foo");
//        obj.put("num", 100);
//        obj.put("balance", 1000.21);
//        obj.put("is_vip", new Boolean(true));
//        System.out.println(obj);
//
//        //{	"name":"John",	"age":30,	"cars":["Ford","BMW","Fiat"]}
//
//        JSONObject obj2 = new JSONObject();
//
//        //"name":"John"
//        obj2.put("name", "John");
//
//        //"age":30
//        obj2.put("age", 30);
//
//        //"cars":["Ford","BMW","Fiat"]
//        JSONArray jaCar = new JSONArray();
//        jaCar.add("Ford");
//        jaCar.add("BMW");
//        jaCar.add("Fiat");
//
//        obj2.put("cars", jaCar);
//
//        System.out.println(obj2);
//
//        /*
//        {
//            "name":"John",
//            "age":30,
//            "motorcycle":{
//                            "Merek":"Honda",
//                            "Tipe":"Vario"
//                        }
//        }
//        */
//
//        JSONObject obj3 = new JSONObject();
//        //"name":"John"
//        obj3.put("name", "John");
//
//        //"age":30
//        obj3.put("age", 30);
//
//        /*"motorcycle":{
//            "Merek":"Honda",
//                    "Tipe":"Vario"
//        }*/
//        JSONObject obj3Sub = new JSONObject();
//        obj3Sub.put("Merek", "Honda");
//        obj3Sub.put("Tipe", "Vario");
//        obj3.put("motorcycle", obj3Sub);
//
//        System.out.println(obj3);
//
//        Obj 4

        JSONArray modFord = new JSONArray();
        modFord.add("Fiesta");
        modFord.add("Focus");
        modFord.add("Mustang");

        JSONObject subObj1 = new JSONObject();

        subObj1.put("name", "Ford");
        subObj1.put("models", modFord);

        JSONArray modBMW = new JSONArray();
        modBMW.add("320");
        modBMW.add("X3");
        modBMW.add("X5");

        JSONObject subObj2 = new JSONObject();

        subObj2.put("name", "BMW");
        subObj2.put("models", modBMW);

        JSONArray modFiat = new JSONArray();
        modFiat.add("500");
        modFiat.add("Panda");

        JSONObject subObj3 = new JSONObject();

        subObj3.put("name", "Fiat");
        subObj3.put("models", modFiat);



        JSONArray cars = new JSONArray();
        cars.add(subObj1);
        cars.add(subObj2);
        cars.add(subObj3);

        JSONObject obj = new JSONObject();
        obj.put("name", "John");
        obj.put("age", 30);
        obj.put("cars", cars);

        System.out.println(obj);
    }
}
