package Day9.Assignment1;

import Day9.Assignment1.MahasiswaBaru;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;
import java.io.IOException;

public class Assignment1Client {

    public static void main(String[] args) throws Exception{
        String ipProp = "";
        String portProp = "";
        Scanner sc = new Scanner(System.in);
        ArrayList<MahasiswaBaru> alMaha = new ArrayList<>();
        MahasiswaBaru mB;

        try {
            try (InputStream input = new FileInputStream("/Users/ada-nb185/Documents/config.txt")) {
                Properties prop = new Properties();

                // load a properties file
                prop.load(input);

                // get the property value and print it out
                ipProp = prop.getProperty("ip");
                portProp = prop.getProperty("port");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            Socket s = new Socket(ipProp, Integer.parseInt(portProp));
            DataInputStream dis = new DataInputStream(s.getInputStream());
            DataOutputStream dout = new DataOutputStream(s.getOutputStream());
            String pesan = "";
            String dataMaha = "";

            while (!pesan.equalsIgnoreCase("EXIT")) {

                    System.out.println("MENU");
                    System.out.println("1. Connect Socket");
                    System.out.println("2. Create FileProses");
                    System.out.println("3. Tulis  ke File, Connect - Kirim FTP");
                    System.out.println("4. Close All Connection");
                    System.out.println("99. EXIT");
                    System.out.println("INPUT NOMOR: ");

                //Send
                pesan = sc.next();
                dout.writeUTF(pesan);
                dout.flush();

                //Receive
                if (pesan.equalsIgnoreCase("EXIT")) {
                    System.out.println("Server exited");
                    break;
                } else if (pesan.equalsIgnoreCase("1")) {
                    dataMaha = (String) dis.readUTF();
                    System.out.println(dataMaha);
                } else if (pesan.equalsIgnoreCase("2")) {
//                    System.out.println(dataMaha);
                    String strAccuParsed[] = dataMaha.split("\n");
                    FileWriter fw = new FileWriter("/Users/ada-nb185/Documents/IdeaProjects/Day9/src/Day9/Assignment1/fileprosesss.txt");

                    for (int j = 0; j < strAccuParsed.length; j++) {
                        String strAccuParsed2[] = strAccuParsed[j].trim().split(",");
                        String nama = strAccuParsed2[0];
                        int nilaiFisika = Integer.parseInt(strAccuParsed2[1]);
                        int nilaiKimia = Integer.parseInt(strAccuParsed2[2]);
                        int nilaiBiologi = Integer.parseInt(strAccuParsed2[3]);

                        mB = new MahasiswaBaru(nama, nilaiFisika, nilaiKimia, nilaiBiologi);
                        alMaha.add(mB);
                    }
//                    System.out.println(alMaha);
                        for (MahasiswaBaru et : alMaha) {
                            fw.write("Nama : " + et.getNama());
                            fw.write("\n");
                            fw.write("Nilai Kimia : " + et.getNilaiKimia());
                            fw.write("\n");
                            fw.write("Nilai Fisika : " + et.getNilaiFisika());
                            fw.write("\n");
                            fw.write("Nilai Biologi : " + et.getNilaiBiologi());
                            fw.write("\n\n");
                        }
                        fw.close();

                } else if (pesan.equalsIgnoreCase("3")) {
                    TulisThread t1 = new TulisThread(alMaha);
                    FTPThread t2 = new FTPThread();
                    t1.start();
                    t2.start();
                } else if (pesan.equalsIgnoreCase("4")) {
                    dout.close();
                    s.close();
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

}

