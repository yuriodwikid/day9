package Day9.Assignment1;

class MahasiswaBaru {
    String nama;
    int nilaiKimia;
    int nilaiFisika;
    int nilaiBiologi;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getNilaiKimia() {
        return nilaiKimia;
    }

    public void setNilaiKimia(int nilaiKimia) {
        this.nilaiKimia = nilaiKimia;
    }

    public int getNilaiFisika() {
        return nilaiFisika;
    }

    public void setNilaiFisika(int nilaiFisika) {
        this.nilaiFisika = nilaiFisika;
    }

    public int getNilaiBiologi() {
        return nilaiBiologi;
    }

    public void setNilaiBiologi(int nilaiBiologi) {
        this.nilaiBiologi = nilaiBiologi;
    }

    MahasiswaBaru(String nama, int nilaiFisika, int nilaiKimia, int nilaiBiologi) {
        this.nama = nama;
        this.nilaiFisika = nilaiFisika;
        this.nilaiKimia = nilaiKimia;
        this.nilaiBiologi = nilaiBiologi;
    }
}