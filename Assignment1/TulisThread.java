package Day9.Assignment1;

import Day9.Assignment1.MahasiswaBaru;

import java.io.FileWriter;
import java.util.ArrayList;

public class TulisThread extends Thread {
    private ArrayList<MahasiswaBaru> alClient = new ArrayList<MahasiswaBaru>();

    public TulisThread(ArrayList<MahasiswaBaru>alClient) {
        this.alClient = alClient;
    }

    public void run () {
        try{
            FileWriter fw = new FileWriter("/Users/ada-nb186/Documents/JavaIntelliJ/Astrapay/FileRata2Reza.txt");

            fw.write("Nama.NilaiRata2\n");
            for (MahasiswaBaru el: alClient) {
                fw.write(el.getNama()+ " " + ((el.getNilaiFisika()+ el.getNilaiKimia()+el.getNilaiBiologi())/3)+"\n");
            }
            fw.close();
        }
        catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Process FileRata2Reza.txt complete");
        System.out.println("Upload Success");
    }
}
